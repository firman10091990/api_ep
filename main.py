from flask import Flask, request, jsonify, json, make_response
import json, time, logging, pandas as pd, numpy as np
from mapr.ojai.storage.ConnectionFactory import ConnectionFactory
from mapr.ojai.ojai_query.QueryOp import QueryOp
from datetime import datetime
from flask import render_template, send_from_directory

app = Flask(__name__, template_folder='swagger_ui/templates', static_folder='swagger_ui/static')

connection_str = "10.91.2.119:8553 ?auth=basic;user=adm-app;password=@Dira2020;ssl=false;sslCA=./ssl_truststore.pem;sslTargetNameOverride=AF-BDSBY04;"
connection = ConnectionFactory.get_connection(connection_str=connection_str)

app.logger.setLevel(logging.DEBUG)

@app.route('/api/EP/PO', methods=['POST'])
def table_PO2():
    
    # try:
    app.logger.info(f"Performing Query")
    
    # Ambil parameter dari POST method
    content = request.get_json()
    id_ = content['id']
    st_date = content['start_date']
    end_date = content['end_date']

    # query to be executed
    st_time = time.time()
    
    store = connection.get_store(store_path='/user/maprdb/ad1survey_table_PO')
    def convOTime(x):
        try:
            if not type(x) == str:
                return str(x.to_date().isoformat())
            else :
                return x
        except:
            return "NULL"
    
    query = {"$select" : ["*"],
                        "$where" : {'$and': 
                                    [
                                        {"$ge" : {"_id" : f'{id_}~{st_date}'}},
                                        {"$le" : {"_id" : f'{id_}~{end_date}%'}},
                                    ]
                                    }
                        }
    options = {
                'ojai.mapr.query.result-as-document': True
    }
        
    raw_data = store.find(query, options=options)
    
    buff = []
    
    for n in raw_data:
        buff.append(n.as_dictionary())
        
    if len(buff)>0:
        buff=pd.DataFrame.from_records(buff)
        
        if "TGL_ORDER" not in buff.columns:
            buff["TGL_ORDER"] = 0
        else:
            buff["TGL_ORDER"] = buff["TGL_ORDER"].apply(convOTime)
        
        #TGL_ORDER = buff["TGL_ORDER"].astype(str)
        #TGL_ORDER1 = TGL_ORDER.replace('T', ' ')
        ##TGL_ORDER2 = TGL_ORDER1.replace('.000Z', '')
        #uff.loc[:, ['TGL_ORDER']] = buff[['TGL_ORDER']]
        print(buff["TGL_ORDER"])
        
        results = json.loads(buff.to_json(orient="records"))
    else:
        results = [] 
        
    end_time = time.time()
    app.logger.info(f"Filtering Data for  : {end_time - st_time}")

    return jsonify({'status':('0' if results ==[] else '1'),'id':id_,'data' : results})

@app.route('/api/EP/PPD', methods=['POST'])
def table_PPD2():
    
    # try:
    app.logger.info(f"Performing Query")
    
    # Ambil parameter dari POST method
    content = request.get_json()
    id_ = content['id']
    st_date = content['start_date']
    end_date = content['end_date']

    # query to be executed
    st_time = time.time()
    
    store = connection.get_store(store_path='/user/maprdb/ad1survey_table_PPD')
    
    def convOTime(x):
        try:
            if not type(x) == str:
                return str(x.to_date().isoformat())
            else :
                return x
        except:
            return "NULL"
    
    query = {"$select" : ["*"],
                        "$where" : {'$and': 
                                    [
                                        {"$ge" : {"_id" : f'{id_}~{st_date}'}},
                                        {"$le" : {"_id" : f'{id_}~{end_date}%'}},
                                    ]
                                    }
                        }
    options = {
                'ojai.mapr.query.result-as-document': True
    }
        
    raw_data = store.find(query, options=options)
    
    buff = []
    
    for n in raw_data:
        buff.append(n.as_dictionary())
        
    if len(buff)>0:
        
        buff=pd.DataFrame.from_records(buff)
        
        if "APPL_DATE" not in buff.columns:
            buff["APPL_DATE"] = 0
        else:
            buff["APPL_DATE"] = buff["APPL_DATE"].apply(convOTime)    
            
        results = json.loads(buff.to_json(orient="records"))
    else:
        results = [] 
        
    end_time = time.time()
    app.logger.info(f"Filtering Data for  : {end_time - st_time}")

    return jsonify({'status':('0' if results ==[] else '1'),'id':id_,'data' : results})

@app.route('/api/EP/OrderMasuk', methods=['POST'])
def table_Order_Masuk2():
    
    # try:
    app.logger.info(f"Performing Query")
    
    # Ambil parameter dari POST method
    content = request.get_json()
    id_ = content['id']
    st_date = content['start_date']
    end_date = content['end_date']

    # query to be executed
    st_time = time.time()
    
    store = connection.get_store(store_path='/user/maprdb/ad1survey_table_Order_Masuk')
    def convOTime(x):
        try:
            if not type(x) == str:
                return str(x.to_date().isoformat())
            else :
                return x
        except:
            return "NULL"
    
    query = {"$select" : ["*"],
                        "$where" : {'$and': 
                                    [
                                        {"$ge" : {"_id" : f'{id_}~{st_date}'}},
                                        {"$le" : {"_id" : f'{id_}~{end_date}%'}},
                                    ]
                                    }
                        }
    options = {
                'ojai.mapr.query.result-as-document': True
    }
        
    raw_data = store.find(query, options=options)
    
    buff = []
    
    for n in raw_data:
        buff.append(n.as_dictionary())
        
    if len(buff)>0:
        buff=pd.DataFrame.from_records(buff)
        
        if "TGL_ORDER" not in buff.columns:
            buff["TGL_ORDER"] = 0
        else:
            buff["TGL_ORDER"] = buff["TGL_ORDER"].apply(convOTime) 
            
        results = json.loads(buff.to_json(orient="records"))
    else:
        results = [] 
        
    end_time = time.time()
    app.logger.info(f"Filtering Data for  : {end_time - st_time}")

    return jsonify({'status':('0' if results ==[] else '1'),'id':id_,'data' : results})


if __name__ == '__main__':
    port = '5060'
    app.run(host='10.91.2.128', port=port, debug=False)