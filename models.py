from marshmallow import Schema, fields

class dataDetailTagihan(Schema):
    TANGGAL_CAIR = fields.Str()
    PEMBAYARAN_LIMIT = fields.Float()
    PEMBAYARAN_KOMISI_LANGSUNG = fields.Float()
    NO_REF = fields.Str()
    UNIT = fields.Str()
    KONSUMEN = fields.Str()
    BANK = fields.Str()
    NO_AKUN = fields.Str()
    NAMA_AKUN = fields.Str()
    JUMLAH = fields.Float()
    KODE_DLC_PARTNER = fields.Str()
    PEMBAYARAN = fields.Str()
    STATUS_PENCAIRAN = fields.Str()

class parentDetailTagihan(Schema):
    status = fields.Str()
    id = fields.Str()
    data = fields.List(fields.Nested(dataDetailTagihan))

class dataSummaryTransaksi(Schema):
    KODE_DLC_PARTNER = fields.Str()
    PROSES_PENAGIHAN = fields.Float()
    PO = fields.Float()
    TOTAL_PEMASUKAN_OWNER_DEALER = fields.Float()
    TOTAL_KOMISI_LANGSUNG = fields.Float()
    TOTAL_PENCAIRAN_UNIT = fields.Float()
    SUDAH_CAIR = fields.Float()

class parentSummaryTransaksi(Schema):
    status = fields.Str()
    id = fields.Str()
    data = fields.List(fields.Nested(dataSummaryTransaksi))

class dataBmHistory(Schema):
    NO_APPL_UNIT = fields.Str()
    TANGGAL = fields.Str()
    JAM = fields.Str()
    STATUS = fields.Str()
    SUB_STATUS = fields.Str()
    Sequence = fields.Str()
    Flag = fields.Str()

class parentBmHistory(Schema):
    status = fields.Str()
    id = fields.Str()
    data = fields.List(fields.Nested(dataBmHistory))

class dataBmTracking(Schema):
    NO_APLIKASI = fields.Str()
    STATUS = fields.Str()
    NAMA = fields.Str()
    OBJECT_GROUP_CODE = fields.Str()
    OBJECT_GROUP_NAME = fields.Str()
    GROUPING_STATUS = fields.Str()
    MODEL_CODE = fields.Str()
    MODEL_NAME = fields.Str()
    Flag = fields.Str()

class parentBmTracking(Schema):
    status = fields.Str()
    id = fields.Str()
    data = fields.List(fields.Nested(dataBmTracking))