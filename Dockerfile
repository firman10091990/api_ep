FROM python:3.7.0-slim

RUN mkdir /api_ad1partner
RUN addgroup --system  flask && adduser --system flask && \
    chown flask.flask /api_ad1partner
RUN python -m pip install --upgrade pip
USER flask:flask
WORKDIR /api_ad1partner
RUN unset http_proxy && \
    unset https_proxy && \
    unset HTTP_PROXY && \
    unset HTTPS_PROXY && \
    pip3 install --upgrade pip setuptools wheel --proxy http://192.100.1.50:8080
ADD requirement_api.txt /api_ad1partner/requirement_api.txt
RUN unset http_proxy && \
    unset https_proxy && \
    unset HTTP_PROXY && \
    unset HTTPS_PROXY && \
    pip3 install --no-cache-dir -r /api_ad1partner/requirement_api.txt --proxy http://192.100.1.50:8080/ --user

COPY ssl_truststore.pem config.json main.py models.py /api_ad1partner/
COPY swagger_ui /api_ad1partner/swagger_ui

EXPOSE 5059

CMD ["python", "/api_ad1partner/main.py"]
